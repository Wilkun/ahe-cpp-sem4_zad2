# AHE Semestr 4 Zadanie 2

Napisz własną implementację klasy complex umożliwiającą wykonywanie operacji
matematycznych na liczbach zespolonych.

Użycie: przeciążania operatorów, zastosowanie wzorców template będzie
dodatkowym atutem (ale nie jest obowiązkowe). Używając własnej implementacji
klasy complex , napisz program rozwiązujący równanie kwadratowe w zbiorze
liczb zespolonych przyjmujący argumenty ze standardowego wejścia.

Klasa complex powinna być umieszczona w przestrzeni nazw o nazwie: imie_nazwisko

Użycie wzorca projektowego jest konieczne, i tylko takie zadania będą przyjmowane.
