//
// Complex.h
// sem4_zad2
//
// Created by Wilkun on 23.11.2019.
// Last edited by Wilkun on 23.11.2019.
//

#ifndef WILKUN_COMPLEX_H
#define WILKUN_COMPLEX_H 1

#include <ostream>
#include <sstream>

namespace Wilkun {
    template<typename T>
    class Complex {
    private:
        T Re;
        T Im;
    public:
        explicit Complex<T>(T _Re = 0.0, T _Im = 0.0)
                : Re(_Re), Im(_Im) {};

        Complex<T>(const Complex<T> &c)
                : Re(c.Re), Im(c.Im) {};

        Complex<T>& operator=(const T& t);

        T real() const;

        T imag() const;

        Complex& operator=(const Complex&) = default;

        template<typename U>
        Complex<T>& operator=(const Complex<U>& _z);

        Complex<T> operator-() const;

        Complex<T> &operator+=(const Complex<T> &c2);

        Complex<T> &operator-=(const Complex<T> &c2);

        Complex<T> &operator*=(const T &_t);

        Complex<T> &operator*=(const Complex<T> &c2);

        Complex<T> &operator/=(const Complex<T> &c2);

        Complex<T> operator+(const Complex<T> &c2) const;

        Complex<T> operator-(const Complex<T> &c2) const;

        Complex<T> operator*(const Complex<T> &c2) const;

        Complex<T> operator*(const T scalar) const;

        Complex<T> operator/(const Complex<T> &c2) const;

        Complex<T> pow() const;

        [[nodiscard]] std::string print() const {
            std::stringstream ss;
            ss << *this;
            return ss.str();
        }

        Complex<T> conjugate() {
            return Complex<T>(Re, -Im);
        }

        template<typename U>
        friend std::ostream &operator<<(std::ostream &os, const Complex<U> &c) {
            if (c.Re != 0)
                os << c.Re;

            if (c.Re == 0) {
                if (c.Im == 0) {
                    os << "0";
                } else {
                    os << c.Im << "i";
                }
            } else {
                if (c.Im < 0) {
                    os << c.Im << "i";
                } else if (c.Im > 0) {
                    os << "+" << c.Im << "i";
                }
            }
            return os;
        };
    };
}

#endif //WILKUN_QUADRATICSOLVER_H
