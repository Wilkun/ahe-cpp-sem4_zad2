//
// View.h
// sem4_zad2
//
// Created by Wilkun on 23.11.2019.
// Last edited by Wilkun on 23.11.2019.
//

#ifndef WILKUN_VIEW_H
#define WILKUN_VIEW_H 1

#include <QuadraticSolver.h>
#include <memory>
#include <map>
#include <functional>
#include <iostream>

namespace Wilkun {
    class View {

    public:
        explicit View() {}

        virtual ~View() {}

        void displayMainMenu();

        void displayQuadraticSolverView();
    };
}

#endif //WILKUN_VIEW_H
