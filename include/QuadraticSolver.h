//
// QuadrativSolver.h
// sem4_zad2
//
// Created by Wilkun on 25.11.2019.
// Last edited by Wilkun on 25.11.2019.
//

#ifndef WILKUN_QUADRATICSOLVER_H
#define WILKUN_QUADRATICSOLVER_H 1

#include <Complex.h>
#include <cmath>
#include <vector>
#include <iostream>

namespace Wilkun {
    template<typename T>
    class QuadraticSolver {
        T a, b, c, discriminant, x0, x1, x2;
        Complex<T> z;
        bool complexRoots{}, singleRoot{};
        std::vector<std::string> resolve{};

        T solve_discriminant();

        T solve_x0();

        T solve_x1();

        T solve_x2();

        Complex<T> solve_z();

        void solve();

    public:
        explicit QuadraticSolver<T>(
                T _a = 1.0,
                T _b = 0.0,
                T _c = 0.0
        ) : a(_a), b(_b), c(_c) {
            this->solve();
        }

        virtual ~QuadraticSolver() = default;

        void displayResolve();

        template<typename U>
        friend std::ostream &operator<<(std::ostream &os, const QuadraticSolver<U> &cp);
    };

    template<typename T>
    T QuadraticSolver<T>::solve_discriminant() {
        return (pow(b, 2.0) - (a * c * 4.0));
    }

    template<typename T>
    T QuadraticSolver<T>::solve_x0() {
        return -b / (2 * a);
    }

    template<typename T>
    T QuadraticSolver<T>::solve_x1() {
        return (-b - sqrt(discriminant)) / (2 * a);
    }

    template<typename T>
    T QuadraticSolver<T>::solve_x2() {
        return (-b + sqrt(discriminant)) / (2 * a);
    }

    template<typename T>
    Complex<T> QuadraticSolver<T>::solve_z() {
        T Re = this->solve_x0();
        T Im = sqrt(-discriminant) / (2 * a);
        return Complex<T>(Re, Im);
    }

    template<typename T>
    void QuadraticSolver<T>::solve() {
        // formula:
        //    discriminant = (b^ - 4ac)
        this->discriminant = this->solve_discriminant();

        resolve.push_back("Delta wynosi: " + std::to_string(this->discriminant));

        if (discriminant > 0) {
            resolve.push_back("Delta jest dodatnie, sa 2 miejsca zerowe.");

            x1 = solve_x1();

            resolve.push_back("X1: " + std::to_string(x1));

            x2 =solve_x2();

            resolve.push_back("X2: " + std::to_string(x2));
        } else if (discriminant < 0) {
            resolve.push_back("Delta jest ujemna, sa 2 miejsca zerowe w zbiorze liczb zespolonych.");

            complexRoots = true;
            z = solve_z();

            resolve.push_back("Z1: " + z.print());
            resolve.push_back("Z2: " + z.conjugate().print() );

        } else {
            resolve.push_back("Delta wynosi 0, jest 1-no miejsce zerowe.");

            this->singleRoot = true;
            x0 = this->solve_x0();

            resolve.push_back("X: " + std::to_string(this->x0));
        }
    }

    template<typename T>
    void QuadraticSolver<T>::displayResolve() {
        for (auto i : resolve)
            std::cout << i << std::endl;
    }

    template<typename U>
    std::ostream &operator<<(std::ostream &os, const QuadraticSolver<U> &cp) {
        os << "a*x2+b*x+c = 0, gdzie a=" << cp.a << ", b=" << cp.b << ", c=" << cp.c << '\n';
        return os;
    }
}

#endif //WILKUN_QUADRATICSOLVER_H
