//
// main.cpp
// sem4_zad2
//
// Created by Wilkun on 23.11.2019.
// Last edited by Wilkun on 23.11.2019.
//

#include <View.h>
#include <memory>

int main() {
    auto view1 = std::make_shared<Wilkun::View>();

    view1->displayMainMenu();

    return 0;
}