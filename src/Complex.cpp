//
// Complex.cpp
// ahe_sem4_zad2
//
// Created by Ireneusz Blicharski on 24.11.2019.
// Last edited by Ireneusz Blicharski on 24.11.2019.
//

#include <Complex.h>

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator=(const T &t) {
    Re = t;
    Im =T();
    return *this;
}

template<typename T>
T Wilkun::Complex<T>::real() const {
    return Re;
}

template<typename T>
T Wilkun::Complex<T>::imag() const {
    return Im;
}

template<typename T>
template<typename U>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator=(const Wilkun::Complex<U> &_z) {
    Re = _z.real();
    Im = _z.imag();
    return *this;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator-() const {
    return Complex(-Re, -Im);
}

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator+=(const Wilkun::Complex<T> &c2) {
    Re += c2.Re;
    Im += c2.Im;
    return *this;
}

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator-=(const Wilkun::Complex<T> &c2) {
    return *this += -c2;
}

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator*=(const T &_t) {
    Re *= _t;
    Im *= _t;
    return *this;
}

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator*=(const Wilkun::Complex<T> &c2) {
    T real = Re * c2.Re - Im * c2.Im;
    T imag = Re * c2.Im + Im * c2.Re;
    Re = real;
    Im = imag;
    return *this;
}

template<typename T>
Wilkun::Complex<T> &Wilkun::Complex<T>::operator/=(const Wilkun::Complex<T> &c2) {
    Complex<T> nm = Complex<T>(*this) * Complex<T>(c2.Re, -c2.Im);
    T dn = c2.Re * c2.Re + c2.Im * c2.Im;
    Re = nm.Re / dn;
    Im = nm.Im / dn;
    return *this;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator+(const Wilkun::Complex<T> &c2) const {
    return Complex<T>(*this) += c2;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator-(const Wilkun::Complex<T> &c2) const {
    return Complex<T>(*this) -= c2;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator*(const Wilkun::Complex<T> &c2) const {
    return Complex<T>(*this) *= c2;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator*(const T scalar) const {
    return Complex<T>(*this) *= scalar;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::operator/(const Wilkun::Complex<T> &c2) const {
    return Complex<T>(*this) /= c2;
}

template<typename T>
Wilkun::Complex<T> Wilkun::Complex<T>::pow() const {
    Complex<T> pow = (*this) * (*this);
    return pow;
}