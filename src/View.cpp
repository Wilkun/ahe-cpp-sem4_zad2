//
// View.cpp
// sem4_zad2
//
// Created by Wilkun on 23.11.2019.
// Last edited by Wilkun on 23.11.2019.
//

#include <View.h>

void Wilkun::View::displayMainMenu() {
    bool run = true;
    std::map<std::string, std::pair<std::string, std::function<void()> > > main_menu;

    main_menu["1"] = std::make_pair<std::string, std::function<void()> >(
            "Oblicz funckje kwadratowo",
            std::bind(&View::displayQuadraticSolverView, this)
    );

    main_menu["exit"] = std::make_pair<std::string, std::function<void()> >(
            "Wyjscie z programu.",
            [&run]() {
                run = false;
                std::cout << "Konczymy\n";
            }
    );

    std::string choice;
    std::cout << "AHE/OOP Obliczanie funcji kwadratowej: \n";
    do {
        std::cout << "Prosze wybrac akcje i nacisnac ENTER: \n";
        auto it = main_menu.begin();
        while (it != main_menu.end()) {
            std::cout << "[" << (it)->first << "] " << (it++)->second.first << std::endl;
        }

        std::cin >> choice;
        if (main_menu.find(choice) == main_menu.end()) {
            /* item isn't found */
            continue; /* next round */
        }
        std::cout << "\n";
        main_menu[choice].second(); /* executes the function */
    } while (run);
    std::cout << "Dziekuje za skorzystanie z programu. \n";
}

void Wilkun::View::displayQuadraticSolverView() {
    double a, b, c;

    std::cout << "Podaj argumenty funkcji kwadratowej." << '\n' << "Wcisnij ENTER po wprowadzeniu danych." << '\n';

    do {
        std::cout << "Parametr a, nie moze wynosic 0." << '\n';
        std::cin >> a;
    } while ( a == 0.0);

    std::cout << "Parametr b." << '\n';
    std::cin >> b;

    std::cout << "Parametr c." << '\n';
    std::cin >> c;

    auto solver = std::make_shared<Wilkun::QuadraticSolver<double>>(a, b, c);

    std::cout << "Obliczana funkcja ma wzor: " << (*solver) << '\n';

    std::cout << "Wynik obliczen wynosi:" << '\n';

    solver->displayResolve();

    std::cout << '\n';
}